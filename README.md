# Thnikk Touchpad

So [this thing](https://www.etsy.com/listing/656367917/megatouch-keypad-for-osu) is pretty cool.
And its firmware is open source. But it's not written in Rust, so it's time to fix that.

This repo contains `adafruit-freetouch`, an as of yet untested implementation of Adafruit's
[FreeTouch](https://github.com/adafruit/Adafruit_FreeTouch) library (based on the PTC (peripheral
touch control) capabilities in some Microchip MCUs) and `touchpad-firmware`, an in-progress
reimplementation of Thnikk's original firmware for the 2K Touchpad, found in
[this](https://github.com/thnikk/touchPad) repository.