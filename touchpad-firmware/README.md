# Thnikk Capacitive Touch Keypad (2K) Firmware

Got one of these recently, and I was extremely pleased to find that the firmware for it is open
source (and can be found [here](https://github.com/thnikk/touchPad)). I've been wanting to get into
embedded development with Rust for a while now, and I thought to myself, "Hey, this would be a
really great place to get started!" Little did I know, I was in for a whole lot more than I
expected.

The original device firmware relies on `Adafruit_FreeTouch.h`, `Keyboard.h`, `FlashAsEEPROM.h`,
`Adafruit_DotStar.h`, and `Adafruit_NeoPixel.h` (though the last one is only used for four key
keypads), as you may have noticed if you visited the repo linked to above. However, I was only able
to find crates for half of these: `apa102-spi` + `smart-leds` for `Adafruit_DotStar.h`
and `keyberon` for `Keyboard.h`. I also pull in a lot of extra stuff (see `Cargo.toml`) since
there's a lot of stuff imported by default by the original firmware that I needed to handle myself,
like `Serial()`.

Thnikk's 2K touch keypad uses an Adafruit Trinket M0 (ATSAMD21E18A chip) as the brains of the
operation and makes use of its PTC (Peripheral Touch Conteroller) capabilities using Adafruit's
FreeTouch library, which is an open source implementation of part of Microchip's QTouch library.
There's no equivalent in the Rust ecosystem currently, and furthermore, the HAL crate for ATSAMD
chips doesn't expose PTC capabilities for any of the chips it targets, despite all of them having
those capabilities. I was confused why that might be, so I looked into how `atsamd-hal` exposes
various parts of the MCUS. I learned that it does this by using `svd2rust` to generate crates based
on SVD files, which is where all the MCU's peripherals are defined. And lo and behold, only a
handful of them had PTC information (specifically SAM{D,E}51 and SAME5{3,4}). Rats. It was going
to be up to me to add it for the other chips, then. So I got my XML game on, opened the datasheet
for the SAMD21 family of chips (because of course I'm going to add it for the chip I'm using first)
and got to work adding a PTC `<peripheral>` section. For this, I leaned heavily on the datasheet and
the FreeTouch repo, which helpfully had `struct` definitions that looked an awful lot like what an
SVD file converted to C would. Cheers to the contributors to FreeTouch and LibrePTC for their
efforts in reverse-engineering QTouch - I wouldn't have even been able to start on this without
being able to follow in their footsteps.

So that's pretty much where this project is now - I've added PTC support for SAMD21 chips in a [fork
of `atsamd-hal`](https://github.com/AuroransSolis/atsamd) (and maybe the rest of the chips
`atsamd-hal` targets eventually), [made a PR](https://github.com/atsamd-rs/atsamd/pull/420) to
`atsamd-hal` for that addition, created a FreeTouch implementation based on my fork, and started
writing firmware for the touchpad based on all of that. I still need to work out something for
storing device settings (which is what the original firmware depends on `FlashAsEEPROM.h` for),
though I've still got to get the important things like ensuring my firmware rewrite even gets the
touchpad to function. More updates to come as I work out this whole embedded development thing.