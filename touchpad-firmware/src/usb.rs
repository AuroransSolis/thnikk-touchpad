use atsamd_hal::{
    clock::GenericClockController,
    gpio::{Pa24, Pa25, PinMode},
    target_device::{PM, USB},
    usb::UsbBus,
};
use trinket_m0::usb_allocator;
use usb_device::class_prelude::UsbBusAllocator;

static mut USB_ALLOCATOR: Option<UsbBusAllocator<UsbBus>> = None;

pub fn init_usb<M1: PinMode, M2: PinMode>(
    usb: USB,
    pm: &mut PM,
    clocks: &mut GenericClockController,
    usb_dm: Pa24<M1>,
    usb_dp: Pa25<M2>,
) -> Option<&'static UsbBusAllocator<UsbBus>> {
    unsafe {
        if USB_ALLOCATOR.is_none() {
            USB_ALLOCATOR = Some(usb_allocator(usb, clocks, pm, usb_dm, usb_dp));
            USB_ALLOCATOR.as_ref()
        } else {
            None
        }
    }
}
