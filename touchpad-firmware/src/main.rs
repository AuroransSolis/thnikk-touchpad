#![no_std]
#![no_main]

mod rgb;
mod touch_keyboard;
mod usb;

use adafruit_freetouch::{
    freetouch::FreeTouch,
    freetouch_config::{FreeTouchConfig, OversampleLevel, SeriesResistor},
};
use apa102_spi::Apa102;
use atsamd_hal::{
    hal::{
        adc::{Channel, OneShot},
        blocking::spi::Write as SpiWrite,
    },
    ptc::Ptc,
    target_device::PTC,
    timer::{Count16, TimerCounter},
};
use keyberon::Class as KbClass;
use panic_halt as _;
use rgb::*;
use touch_keyboard::TouchKeyboard;
use trinket_m0::{
    clock::GenericClockController,
    entry,
    pac::{CorePeripherals, Peripherals},
    prelude::*,
    UsbBus,
};
use usb::*;
use usb_device::device::{UsbDevice, UsbDeviceBuilder, UsbVidPid};
use usbd_serial::SerialPort;

const FREETOUCH_CONFIG: FreeTouchConfig = FreeTouchConfig::new()
    .oversample(OversampleLevel::Oversample8)
    .series_resistor(SeriesResistor::Res50k);

const INIT_MAPPING: [i32; 3] = [122, 120, 177];

struct GlobalData<FT1, FT2, FT3, RgbTimer, ApaSpi>
where
    FT1: Channel<PTC, ID = u8>,
    Ptc<PTC>: OneShot<PTC, u16, FT1>,
    FT2: Channel<PTC, ID = u8>,
    Ptc<PTC>: OneShot<PTC, u16, FT2>,
    FT3: Channel<PTC, ID = u8>,
    Ptc<PTC>: OneShot<PTC, u16, FT3>,
    RgbTimer: Count16,
    ApaSpi: SpiWrite<u8>,
{
    ft1: FreeTouch<FT1>,
    ft2: FreeTouch<FT2>,
    ft3: FreeTouch<FT3>,
    ptc: Ptc<PTC>,
    rgb: Rgb<RgbTimer, ApaSpi>,
    usb_dev: UsbDevice<'static, UsbBus>,
    usb_kb_class: KbClass<'static, UsbBus, ()>,
    usb_serial_class: SerialPort<'static, UsbBus>,
    keyboard: TouchKeyboard,
}

#[entry]
fn main() -> ! {
    // Basic device setup
    let mut peripherals = Peripherals::take().unwrap();
    let core = CorePeripherals::take().unwrap();
    let mut pins = trinket_m0::Pins::new(peripherals.PORT);
    let mut clocks = GenericClockController::with_internal_32kosc(
        peripherals.GCLK,
        &mut peripherals.PM,
        &mut peripherals.SYSCTRL,
        &mut peripherals.NVMCTRL,
    );

    // DotStar LED setup
    let gclk0 = clocks.gclk0();
    let tc4tc5 = clocks.tc4_tc5(&gclk0).unwrap();
    let led_timer = TimerCounter::tc4_(&tc4tc5, peripherals.TC4, &mut peripherals.PM);
    let led_clock_pin = pins.dotstar_ci.into_push_pull_output(&mut pins.port);
    let led_data_pin = pins.dotstar_di.into_push_pull_output(&mut pins.port);
    let led_not_connected = pins.dotstar_nc.into_floating_input(&mut pins.port);
    let mut led_spi = bitbang_hal::spi::SPI::new(
        apa102_spi::MODE,
        led_not_connected,
        led_data_pin,
        led_clock_pin,
        led_timer,
    );

    // FreeTouch setup
    // Touch pins are d3 (PA07), d4 (PA06), and d1 (PA02).
    let ft_pin_1 = pins.d3.into_function_b(&mut pins.port);
    let ft1 = FreeTouch::new(ft_pin_1, FREETOUCH_CONFIG);
    let ft_pin_2 = pins.d4.into_function_b(&mut pins.port);
    let ft2 = FreeTouch::new(ft_pin_2, FREETOUCH_CONFIG);
    let ft_pin_3 = pins.d1.into_function_b(&mut pins.port);
    let ft3 = FreeTouch::new(ft_pin_3, FREETOUCH_CONFIG);

    // USB setup
    let usb_allocator = init_usb(
        peripherals.USB,
        &mut peripherals.PM,
        &mut clocks,
        pins.usb_dm,
        pins.usb_dp,
    )
    .unwrap();

    // USB interfaces setup
    let usb_dev = UsbDeviceBuilder::new(usb_allocator, UsbVidPid(0x239a, 0x801e))
        .manufacturer("thnikk")
        .product("touchPad")
        .composite_with_iads()
        .build();
    let usb_serial_class = SerialPort::new(usb_allocator);
    let usb_kb_class = keyberon::new_class(usb_allocator, ());

    // Get config from nonvolatile memory
    // todo

    // Touch keyboard init
    let keyboard = TouchKeyboard::new(
        touch_keyboard::DEFAULT_KEY_MAP,
        touch_keyboard::DEFAULT_THRESHOLDS,
    );

    // Final tasks before entering loop
    led_spi.access_timer(|mut timer| {
        timer.start(5.khz());
        timer
    });
    let dotstar = Apa102::new(led_spi);
    let rgb = Rgb::new_tc5(
        RgbMode::Cycle(0),
        dotstar,
        &tc4tc5,
        peripherals.TC5,
        &mut peripherals.PM,
    );
    let ptc = Ptc::ptc(peripherals.PTC, &mut peripherals.PM, &mut clocks);
    let global_data = GlobalData {
        ft1,
        ft2,
        ft3,
        ptc,
        rgb,
        usb_dev,
        usb_kb_class,
        usb_serial_class,
        keyboard,
    };
    loop {
        // wow a lot of stuff goes in here
    }
}
