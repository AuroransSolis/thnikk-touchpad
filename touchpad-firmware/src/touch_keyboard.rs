use adafruit_freetouch::freetouch::FreeTouch;
use atsamd_hal::{
    hal::adc::{Channel, OneShot},
    ptc::Ptc,
    target_device::PTC,
};
use core::array::IntoIter;
use keyberon::key_code::{KbHidReport, KeyCode};

pub const DEFAULT_THRESHOLDS: [u16; 3] = [550, 500, 550];
pub const DEFAULT_KEY_MAP: [(Option<KeyCode>, KeyCode); 3] = [
    (None, KeyCode::F),
    (None, KeyCode::J),
    (None, KeyCode::Escape),
];

pub struct TouchKeyboard {
    key_map: [(Option<KeyCode>, KeyCode); 3],
    press_thresholds: [u16; 3],
    pressed: [bool; 3],
    pressed_lock: [bool; 3],
}

impl TouchKeyboard {
    pub fn new(key_map: [(Option<KeyCode>, KeyCode); 3], press_thresholds: [u16; 3]) -> Self {
        TouchKeyboard {
            key_map,
            pressed: [false; 3],
            pressed_lock: [false; 3],
            press_thresholds,
        }
    }

    pub fn update<FT1, FT2, FT3>(
        &mut self,
        ptc: &mut Ptc<PTC>,
        ft1: &mut FreeTouch<FT1>,
        ft2: &mut FreeTouch<FT2>,
        ft3: &mut FreeTouch<FT3>,
    ) where
        FT1: Channel<PTC, ID = u8>,
        Ptc<PTC>: OneShot<PTC, u16, FT1>,
        FT2: Channel<PTC, ID = u8>,
        Ptc<PTC>: OneShot<PTC, u16, FT2>,
        FT3: Channel<PTC, ID = u8>,
        Ptc<PTC>: OneShot<PTC, u16, FT3>,
    {
        self.update_key(ptc, 0, ft1);
        self.update_key(ptc, 1, ft2);
        self.update_key(ptc, 2, ft3);
    }

    fn update_key<FT>(&mut self, ptc: &mut Ptc<PTC>, key: usize, ft: &mut FreeTouch<FT>)
    where
        FT: Channel<PTC, ID = u8>,
        Ptc<PTC>: OneShot<PTC, u16, FT>,
    {
        let ptc_reading = ft.measure(ptc);
        if ptc_reading > self.press_thresholds[key] {
            self.pressed[key] = true;
        } else if ptc_reading < self.press_thresholds[key] - 50 {
            self.pressed[key] = false;
        }
    }

    pub fn report(&mut self) -> KbHidReport {
        self.key_map
            .iter()
            .zip(self.pressed.iter())
            .zip(self.pressed_lock.iter_mut())
            .filter_map(|((map, pressed), lock)| match (*pressed, *lock) {
                (true, true) => {
                    *lock = false;
                    Some(*map)
                }
                (false, false) => {
                    *lock = true;
                    Some(*map)
                }
                _ => None,
            })
            .flat_map(|(modifier, key)| IntoIter::new([modifier, Some(key)]).flatten())
            .collect()
    }

    pub fn pressed(&self) -> [bool; 3] {
        self.pressed
    }

    pub fn pressed_lock(&self) -> [bool; 3] {
        self.pressed_lock
    }
}
