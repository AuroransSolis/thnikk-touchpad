use crate::touch_keyboard::TouchKeyboard;
use apa102_spi::Apa102;
use atsamd_hal::{
    clock::{Tc4Tc5Clock, Tcc2Tc3Clock},
    hal::{blocking::spi::Write as SpiWrite, timer::CountDown},
    target_device::{PM, TC3, TC4, TC5},
    time::{Milliseconds, Nanoseconds},
};
use core::{array::IntoIter, cmp::Ordering};
use smart_leds::SmartLedsWrite;
use smart_leds_trait::RGB8;
use trinket_m0::timer::{Count16, TimerCounter};

pub struct Rgb<Timer: Count16, Spi: SpiWrite<u8>> {
    apa102: Apa102<Spi>,
    mode: RgbMode,
    rgb: RGB8,
    timer: TimerCounter<Timer>,
}

impl<Spi: SpiWrite<u8>> Rgb<TC3, Spi> {
    pub fn new_tc3(
        mode: RgbMode,
        apa102: Apa102<Spi>,
        clock: &Tcc2Tc3Clock,
        tc: TC3,
        pm: &mut PM,
    ) -> Self {
        let mut timer = TimerCounter::tc3_(clock, tc, pm);
        timer.start(mode.timer_interval());
        Rgb {
            apa102,
            mode,
            rgb: RGB8::from([0; 3]),
            timer,
        }
    }
}

impl<Spi: SpiWrite<u8>> Rgb<TC4, Spi> {
    pub fn new_tc4(
        mode: RgbMode,
        apa102: Apa102<Spi>,
        clock: &Tc4Tc5Clock,
        tc: TC4,
        pm: &mut PM,
    ) -> Rgb<TC4, Spi> {
        let mut timer = TimerCounter::tc4_(clock, tc, pm);
        timer.start(mode.timer_interval());
        Rgb {
            apa102,
            mode,
            rgb: RGB8::from([0; 3]),
            timer,
        }
    }
}

impl<Spi: SpiWrite<u8>> Rgb<TC5, Spi> {
    pub fn new_tc5(
        mode: RgbMode,
        apa102: Apa102<Spi>,
        clock: &Tc4Tc5Clock,
        tc: TC5,
        pm: &mut PM,
    ) -> Rgb<TC5, Spi> {
        let mut timer = TimerCounter::tc5_(clock, tc, pm);
        timer.start(mode.timer_interval());
        Rgb {
            apa102,
            mode,
            rgb: RGB8::from([0; 3]),
            timer,
        }
    }
}

impl<Timer: Count16, Spi: SpiWrite<u8>> Rgb<Timer, Spi> {
    pub fn apply_mode<Apa: SpiWrite<u8>>(
        &mut self,
        keyboard: &TouchKeyboard,
        apa: &mut Apa102<Apa>,
    ) {
        match &mut self.mode {
            RgbMode::Cycle(cycle_count) => {
                if self.timer.wait().is_ok() {
                    let pressed = keyboard.pressed();
                    if pressed[2] {
                        set_rgb(&mut self.rgb, [0; 3]);
                    } else if pressed[0] || pressed[1] {
                        set_rgb(&mut self.rgb, [!0; 3]);
                    } else {
                        if cfg!(debug_assertions) {
                            *cycle_count = cycle_count.wrapping_add(1);
                        } else {
                            *cycle_count += 1;
                        }
                        set_rgb(&mut self.rgb, rgb_from_cycle(*cycle_count));
                    }
                }
            }
            RgbMode::Reactive { flip, step, cycle } => {
                if self.timer.wait().is_ok() {
                    let pressed = keyboard.pressed();
                    if pressed[0] ^ *flip || pressed[1] ^ *flip {
                        *step = 1;
                        *cycle = 169;
                        set_rgb(&mut self.rgb, [!0; 3]);
                    }
                    if *step == 1 {
                        self.rgb.b -= 1;
                        self.rgb.g -= 1;
                        if self.rgb.b == 0 {
                            *step = 2;
                        }
                    } else if *step == 2 {
                        if self.rgb.b == 255 {
                            *step = 3;
                        }
                        set_rgb(&mut self.rgb, rgb_from_cycle(*cycle));
                        *cycle -= 1;
                    } else if *step == 3 {
                        if self.rgb.b != 0 {
                            self.rgb.b -= 1;
                        }
                    }
                }
            }
            RgbMode::SetColour(colour) => set_rgb(&mut self.rgb, *colour),
            RgbMode::FastCycle(cycle) => {
                if self.timer.wait().is_ok() {
                    set_rgb(&mut self.rgb, rgb_from_cycle(*cycle));
                    *cycle += 1;
                }
            }
            RgbMode::Bps {
                led_intervals,
                buffer,
                count,
                change_val,
            } => {
                let pressed = keyboard.pressed();
                if pressed[0] || pressed[1] {
                    *count += 1;
                }
                if *led_intervals >= 1000 {
                    let rgb = if cfg!(debug_assertions) {
                        rgb_from_cycle(count.wrapping_mul(*change_val))
                    } else {
                        rgb_from_cycle(*count * *change_val)
                    };
                    *count = 0;
                    set_rgb(&mut self.rgb, rgb);
                }
                if self.timer.wait().is_ok() {
                    buffer
                        .iter_mut()
                        .zip(self.rgb.as_ref())
                        .map(|(buf, rgb)| (diff_from(rgb.cmp(buf)), buf))
                        .for_each(|(diff, buf)| {
                            if cfg!(debug_assertions) {
                                *buf = buf.wrapping_add(diff);
                            } else {
                                *buf += diff;
                            }
                        });
                    *led_intervals += 1;
                    set_rgb(&mut self.rgb, *buffer);
                }
            }
            RgbMode::ColourChange {
                cycle,
                change,
                change_val,
            } => {
                if !*change {
                    set_rgb(&mut self.rgb, rgb_from_cycle(*cycle));
                    *change = true;
                } else {
                    let count = keyboard.pressed()[0..2]
                        .iter()
                        .zip(keyboard.pressed_lock()[0..2].iter())
                        .filter(|&(&pressed, &lock)| pressed && lock)
                        .count() as u8;
                    *cycle += if cfg!(debug_assertions) {
                        count.wrapping_mul(*change_val)
                    } else {
                        count * *change_val
                    };
                    set_rgb(&mut self.rgb, rgb_from_cycle(*cycle));
                }
            }
            RgbMode::CustomColour(colour) => {
                if self.timer.wait().is_ok() {
                    let pressed = keyboard.pressed();
                    if pressed[2] {
                        set_rgb(&mut self.rgb, [0; 3]);
                    } else if pressed[0] || pressed[1] {
                        set_rgb(&mut self.rgb, [!0; 3]);
                    } else {
                        set_rgb(&mut self.rgb, *colour);
                    }
                }
            }
        }
        apa.write(IntoIter::new([self.rgb])).unwrap_or(());
    }
}

fn set_rgb(rgb: &mut RGB8, [r, g, b]: [u8; 3]) {
    rgb.r = r;
    rgb.g = g;
    rgb.b = b;
}

fn diff_from(ord: Ordering) -> u8 {
    match ord {
        Ordering::Less => 0b11111111, // -1
        Ordering::Equal => 0,
        Ordering::Greater => 1,
    }
}

pub enum RgbMode {
    Cycle(u8),
    Reactive {
        flip: bool,
        step: u8,
        cycle: u8,
    },
    SetColour([u8; 3]),
    FastCycle(u8),
    Bps {
        led_intervals: u16,
        buffer: [u8; 3],
        count: u8,
        change_val: u8,
    },
    ColourChange {
        cycle: u8,
        change: bool,
        change_val: u8,
    },
    CustomColour([u8; 3]),
}

impl RgbMode {
    fn timer_interval(&self) -> Nanoseconds {
        match self {
            RgbMode::Cycle(_) | RgbMode::CustomColour(_) => Milliseconds(10),
            RgbMode::Reactive { .. } | RgbMode::FastCycle(_) | RgbMode::Bps { .. } => {
                Milliseconds(1)
            }
            RgbMode::SetColour(_) | RgbMode::ColourChange { .. } => Milliseconds(0),
        }
        .into()
    }
}

fn rgb_from_cycle(cycle: u8) -> [u8; 3] {
    if cycle < 85 {
        let rgb_val = cycle * 3;
        [0, rgb_val, !rgb_val]
    } else if cycle < 170 {
        let rgb_val = (cycle - 85) * 3;
        [rgb_val, !rgb_val, 0]
    } else {
        let rgb_val = (cycle - 170) * 3;
        [!rgb_val, 0, rgb_val]
    }
}
