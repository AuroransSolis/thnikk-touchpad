use crate::freetouch_config::FreeTouchConfig;
use atsamd_hal::{ptc::Ptc, target_device::PTC};
use embedded_hal::adc::{Channel, OneShot};

/// Wrapper around [`Ptc<PTC>`].
///
/// [`Ptc<PTC>`]: atsamd_hal::ptc::Ptc#impl
pub struct FreeTouch<Pin>
where
    Pin: Channel<PTC, ID = u8>,
    Ptc<PTC>: OneShot<PTC, u16, Pin>,
{
    pub pin: Pin,
    pub config: FreeTouchConfig,
}

impl<Pin> FreeTouch<Pin>
where
    Pin: Channel<PTC, ID = u8>,
    Ptc<PTC>: OneShot<PTC, u16, Pin>,
{
    pub fn new(pin: Pin, config: FreeTouchConfig) -> Self {
        Self { pin, config }
    }

    /// Take a measurement without accounting for oversampling.
    pub fn measure_raw(&mut self, ptc: &mut Ptc<PTC>) -> u16 {
        ptc.oversample(self.config.oversample.into());
        ptc.series_resistance(self.config.series_resistor.into());
        self.config
            .frequency_hops
            .map(|hops| ptc.sample_delay(hops.into()));
        ptc.compcap(0x2000);
        ptc.intcap(0x3f);
        ptc.read(&mut self.pin).unwrap_or(!0)
    }

    /// Take a measurement accounting for oversampling.
    pub fn measure(&mut self, ptc: &mut Ptc<PTC>) -> u16 {
        self.measure_raw(ptc) >> (self.config.oversample as u16)
    }
}
