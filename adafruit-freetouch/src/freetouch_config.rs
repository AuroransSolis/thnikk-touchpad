use atsamd_hal::target_device::ptc::{
    convctrl::ADCACCUM_A, freqctrl::SAMPLEDELAY_A, serres::RESISTOR_A,
};

macro_rules! equiv_enum {
    (
        $(#[$meta:meta])*
        enum $enum:ident => $maps_to:tt {
            $($new_var:ident => $maps_to_var:tt),+
        }
    ) => {
        $(#[$meta])*
        #[derive(Copy, Clone)]
        pub enum $enum {
            $($new_var),+
        }

        impl From<$enum> for $maps_to {
            fn from(other: $enum) -> $maps_to {
                match other {
                    $($enum::$new_var => $maps_to::$maps_to_var),+
                }
            }
        }
    }
}

equiv_enum! {
    #[repr(u16)]
    #[doc = "Number of samples to collect and sum per report."]
    enum OversampleLevel => ADCACCUM_A {
        Oversample1 => OVERSAMPLE1,
        Oversample2 => OVERSAMPLE2,
        Oversample4 => OVERSAMPLE4,
        Oversample8 => OVERSAMPLE8,
        Oversample16 => OVERSAMPLE16,
        Oversample32 => OVERSAMPLE32,
        Oversample64 => OVERSAMPLE64
    }
}

equiv_enum! {
    #[doc = "Frequency hopping setting to use. Information on what frequency hopping is can be "]
    #[doc = "found [here](https://microchipsupport.force.com/s/article/What-is-frequency-hopping-in-QTouch-PTC-touch-acquisition)"]
    enum FreqHop => SAMPLEDELAY_A {
        FreqHop1 => FREQHOP1,
        FreqHop2 => FREQHOP2,
        FreqHop3 => FREQHOP3,
        FreqHop4 => FREQHOP4,
        FreqHop5 => FREQHOP5,
        FreqHop6 => FREQHOP6,
        FreqHop7 => FREQHOP7,
        FreqHop8 => FREQHOP8,
        FreqHop9 => FREQHOP9,
        FreqHop10 => FREQHOP10,
        FreqHop11 => FREQHOP11,
        FreqHop12 => FREQHOP12,
        FreqHop13 => FREQHOP13,
        FreqHop14 => FREQHOP14,
        FreqHop15 => FREQHOP15,
        FreqHop16 => FREQHOP16
    }
}

equiv_enum! {
    #[doc = "Series resistor to use on the line being polled."]
    enum SeriesResistor => RESISTOR_A {
        Res0 => RES0,
        Res20k => RES20K,
        Res50k => RES50K,
        Res100k => RES100K
    }
}

/// Configuration struct for FreeTouch pins.
#[derive(Copy, Clone)]
pub struct FreeTouchConfig {
    pub(crate) oversample: OversampleLevel,
    pub(crate) series_resistor: SeriesResistor,
    pub(crate) frequency_hops: Option<FreqHop>,
}

impl FreeTouchConfig {
    pub const fn new() -> Self {
        Self {
            oversample: OversampleLevel::Oversample1,
            series_resistor: SeriesResistor::Res0,
            frequency_hops: None,
        }
    }

    pub const fn oversample(mut self, oversample: OversampleLevel) -> Self {
        self.oversample = oversample;
        self
    }

    pub const fn series_resistor(mut self, series_resistor: SeriesResistor) -> Self {
        self.series_resistor = series_resistor;
        self
    }

    pub const fn frequency_hops(mut self, frequency_hops: FreqHop) -> Self {
        self.frequency_hops = Some(frequency_hops);
        self
    }

    pub const fn no_frequency_hops(mut self) -> Self {
        self.frequency_hops = None;
        self
    }
}
