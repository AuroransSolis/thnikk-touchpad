# FreeTouch

Based on [Adafruit's FreeTouch implementation](https://github.com/adafruit/Adafruit_FreeTouch) and
my own fork of `atsamd-hal`, which exposes the PTC function on SAMD21 MCUs (and others too
eventually, I hope).

This crate provides the `FreeTouch` and `FreeTouchConfig` types, which form a wrapper around
`atsamd_hal::samd21::ptc::Ptc<atsamd_hal::target_device::PAC>` (`target_device` being `atsamd21e`
currently, though I'd like to make generic across SAMD21 devices and eventually all MCUs targeted by
`atsamd-hal`). The latter acts as, unsurprisingly, a set of configuration options when taking
measurements from the PTC, including the oversample rate, internal series resistance, and whether to
enable frequency hopping.